[![npm](https://img.shields.io/npm/v/@ioniczoo/chameleon-mask-directive.svg)](https://www.npmjs.com/package/@ioniczoo/chameleon-mask-directive)
[![npm](https://img.shields.io/npm/dt/@ioniczoo/chameleon-mask-directive.svg)](https://www.npmjs.com/package/@ioniczoo/chameleon-mask-directive)
[![npm](https://img.shields.io/npm/l/@ioniczoo/chameleon-mask-directive.svg?style=flat-square)](https://www.npmjs.com/package/@ioniczoo/chameleon-mask-directive)

<img src="https://gitlab.com/IonicZoo/chameleon-mask-directive/raw/master/img.jpg" width="20%" height="auto" alt="bird" title="bird">

# Camaleão Mask Directive

Maks Directive for Ionic Designed to Work with Numbers: The chameleon, with its adaptability, provides you with a numerical mask for any format you want through a Directive.


## Install

```bash
npm install @ioniczoo/chameleon-mask-directive --save
```

## Import

```ts
import { NgModule } from '@angular/core';
import { MaskDirectiveModule } from '@ioniczoo/chameleon-mask-directive';

@NgModule({
  imports: [
    MaskDirectiveModule
  ],
  declarations: []
})
export class AppModule {}
```

## Example

```html
<!-- Phone Mask -->
<ion-input type="text" mask="(**) * ****-****"></ion-input>
```

## Author

[JP Rodrigues](https://gitlab.com/jprodrigues70)

## Contribute

[Create issues and request pull-request.](https://gitlab.com/IonicZoo/chameleon-mask-directive/blob/master/CONTRIBUTING.md)

## License

[GNU General Public License v3.0](https://gitlab.com/IonicZoo/chameleon-mask-directive/blob/master/LICENSE)
